<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Demo</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>
<body>
    <nav>
        <div class="nav-wrapper teal">
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="sass.html">Requests <span class="new badge yellow lighten-3 black-text">34</span></a></li>
                <li><a href="sass.html">Notifications</a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id="dropdown1" class="dropdown-content">
                        <li><a href="#!">one</a></li>
                        <li><a href="#!">two</a></li>
                        <li class="divider"></li>
                        <li><a href="#!">three</a></li>
                    </ul>
                </li>
                <li><a href="collapsible.html">Logout</a></li>
            </ul>
        </div>
    </nav>

    <ul class="sidenav sidenav-fixed">
        <li>
            <div class="user-view">
                <div class="background">
                    <img src="{{ asset('img/office.jpg') }}">
                </div>
                <a href="#user"><img class="circle" src="{{ asset('img/user.png') }}"></a>
                <a href="#name"><span class="white-text name">John Doe</span></a>
                <a href="#email"><span class="white-text email">jdandturk@gmail.com</span></a>
            </div>
        </li>
        <li class="active"><a class="waves-effect waves-teal" href="#!">Dashboard</a></li>
        <li><a class="waves-effect waves-teal" href="#!">Tasks</a></li>
        <li><a class="waves-effect waves-teal" href="#!">Teams</a></li>
        <li><a class="waves-effect waves-teal" href="#!">Conversations<span class="new badge">4</span></a></li>
        <li><a class="waves-effect waves-teal" href="#!">Calendar</a></li>
    </ul>
</body>
</html>
